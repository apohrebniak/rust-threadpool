use std::sync::Arc;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering;
use std::sync::Condvar;
use std::sync::Mutex;
use std::thread;
use std::thread::JoinHandle;
use std::time::Duration;

type Job = Box<dyn FnBox + Send + 'static>;

trait FnBox {
    fn call(self: Box<Self>);
}

impl<F: FnOnce()> FnBox for F {
    fn call(self: Box<F>) {
        (*self)();
    }
}

struct Worker {
    running: Arc<AtomicBool>,
    thread: Option<JoinHandle<()>>,
}

impl Worker {
    fn new(cond: Arc<Condvar>, job_queue: Arc<Mutex<Vec<Job>>>) -> Worker {
        let running = Arc::new(AtomicBool::new(true));
        let running_ = Arc::clone(&running);

        let thread = thread::spawn(move || {
            'main: loop {
                let mut job_queue_lock = job_queue.lock().unwrap();
                while job_queue_lock.is_empty() {
                    if running_.load(Ordering::Acquire) {
                        job_queue_lock = cond.wait(job_queue_lock).unwrap();
                    } else {
                        break 'main;
                    }
                }
                let job = job_queue_lock.remove(0);

                drop(job_queue_lock);

                job.call();
            }
        });

        Worker { running, thread: Some(thread) }
    }
}

struct Pool {
    jobs: Arc<Mutex<Vec<Job>>>,
    workers: Vec<Worker>,
    cond: Arc<Condvar>,
}

impl Pool {
    fn new(size: usize) -> Pool {
        let jobs = Arc::new(Mutex::new(Vec::new()));
        let mut workers = Vec::with_capacity(size);
        let cond = Arc::new(Condvar::new());

        for _ in 0..size {
            let worker = Worker::new(Arc::clone(&cond), Arc::clone(&jobs));
            workers.push(worker);
        }

        Pool { jobs, workers, cond }
    }

    fn submit(&self, job: Job) {
        let mut lock = self.jobs.lock().unwrap();
        lock.push(job);
        self.cond.notify_all();
    }

    fn shutdown(&mut self) {
        for worker in &mut self.workers {
            worker.running.store(false, Ordering::Relaxed);
        }

        self.cond.notify_all();

        for worker in &mut self.workers {
            if let Some(thread) = worker.thread.take() {
                thread.join().unwrap();
            }
        }
    }
}

impl Drop for Pool {
    fn drop(&mut self) {
        self.shutdown();
    }
}

fn main() {
    let pool = Pool::new(10000);

    pool.submit(Box::new(|| { println!("Here"); }));
    thread::sleep(Duration::from_millis(2000));
    pool.submit(Box::new(|| { println!("Is"); }));
    thread::sleep(Duration::from_millis(1000));
    pool.submit(Box::new(|| { println!("Johny!!!!"); }));
}